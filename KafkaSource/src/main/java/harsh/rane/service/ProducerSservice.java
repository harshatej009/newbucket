package harsh.rane.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import harsh.rane.exception.ProducerException;

@Service
public class ProducerSservice {
	
	public void sendmessage() {
	Properties props=new Properties();
	props.put("bootstrap.servers","0.0.0.0:9092");
	props.put("key.serializer",StringSerializer.class);
	props.put("value.serializer",StringSerializer.class);
	
	Producer <String,String> producer=new KafkaProducer<>(props);
	ProducerRecord<String, String> records=new ProducerRecord<>("topic2", 0,"zkey", "O message Message");
  	producer.send(records, new ProducerException());
  	producer.close();
	
	System.out.println("StringMessage has been sent");
}}
