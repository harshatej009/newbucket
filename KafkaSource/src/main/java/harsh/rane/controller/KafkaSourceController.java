package harsh.rane.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import harsh.rane.dao.PageView;
import harsh.rane.exception.ProducerException;
import harsh.rane.model.Employee;
import harsh.rane.service.EmployeeDaoServiceImpl;
import harsh.rane.service.PatientDaoServiceImpl;

@RestController
public class KafkaSourceController {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplateString;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplateObject;

	@Autowired
	EmployeeDaoServiceImpl empolyeeservice;
	
	@Autowired
	Employee employee;

	@Autowired
	PatientDaoServiceImpl patientservice;

	private static final Logger LOGGER = LogManager.getLogger(KafkaSourceController.class);
	
	
	@GetMapping("/generatestream") // 8082 port
	public void GenerateStream() {

		Properties props = new Properties();
		props.put("bootstrap.servers", "0.0.0.0:9092");
		props.put("key.serializer", StringSerializer.class);
		props.put("value.serializer", StringSerializer.class);
		LOGGER.info("message sending");
		
		kafkaTemplateString.send("topic3", "1", "hey wassup?");
		kafkaTemplateString.send("topic3", "2", "nothing dude"); 
		
		System.out.print("sent2");

		LOGGER.info("StringMessage has been sent");
	}
}
